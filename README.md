C++ class - wrapper to call REST API requests over http and https protocols.

##Dependencies
boost/beast (basis implementation of http and https client functionality).
openssl (to support https)
catch2 (for unit tests).

##RoadMap
Version 0.1
- support for https GET requests.
- noce to have: support for http GET requests
- no support for authentication.
- possibility to set custom headers for requests.

##Test cases 

1. Perform Get request on url https://api.iextrading.com:443/1.0/stock/aapl/price and get current price for Apple stocks.
2. Perform GET request on http://api.db-ip.com/v2/free/64.34.157.170 and get information about location
3. Perform GET request on https://api.db-ip.com/v2/free/64.34.157.170 and get information about location.

